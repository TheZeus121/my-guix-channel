(define-module (ukko packages ebook)
  #:use-module (gnu packages)
  #:use-module ((gnu packages compression) #:prefix gnu:)
  #:use-module ((gnu packages multiprecision) #:prefix gnu:)
  #:use-module (guix build-system gnu)
  #:use-module (guix download)
  #:use-module ((guix licenses) #:prefix license:)
  #:use-module (guix packages))

(define-public convlit
  ;; XXX: when building there's a lot of errors about char signedness
  (package
    (name "convlit")
    (version "1.8")
    (source (origin
              (method url-fetch)
              (uri "http://www.convertlit.com/convertlit18src.zip")
              (sha256
               (base32
                "1fjpwncyc2r3ipav7c9m7jxy6i7mphbyqj3gsm046425p7sqa2np"))))
    (build-system gnu-build-system)
    (native-inputs
     `(("libtommath" ,gnu:libtommath "static") ;XXX: fix the staticness
       ("unzip" ,gnu:unzip)))
    (arguments
     `(#:tests? #f
       #:phases
       (modify-phases %standard-phases
         (replace 'configure
           (lambda* (#:key inputs #:allow-other-keys)
             (substitute* "../clit18/Makefile"
               (("../libtommath-0.30")
                (string-append (assoc-ref inputs "libtommath")
                               "/lib")))))
         (add-after 'build 'build-lib
           (lambda _
             (chdir "../lib")
             (invoke "make" "CC=gcc")))
         (add-after 'build-lib 'build-clit
           (lambda _
             (chdir "../clit18")
             (invoke "make" "CC=gcc")))
         (delete 'build)
         (replace 'install
           (lambda* (#:key outputs #:allow-other-keys)
             (install-file "clit" (string-append (assoc-ref outputs "out")
                                                 "/bin/"))
             #t)))))
    (home-page "http://www.convertlit.com/")
    (synopsis "An extractor/converter for .LIT eBooks")
    (description
     "Convert LIT is a program which allows you to convert Microsoft
Reader format eBooks into open format for use with software or devices
which are not directly compatible with Microsoft's Reader.")
    (license license:gpl2+)))

;; Renamed to Debian name
(define-public convertlit
  (deprecated-package "convertlit" convlit))
