(define-module (ukko packages emacs)
  #:use-module ((gnu packages emacs-xyz) #:prefix gnu:)
  #:use-module ((gnu packages ruby) #:prefix gnu:)
  #:use-module (guix build-system emacs)
  #:use-module (guix download)
  #:use-module (guix git-download)
  #:use-module ((guix licenses) #:prefix license:)
  #:use-module (guix packages))

(define-public emacs-auto-compile-master
  (let ((commit "3b4d94b020a2557e439233dbaa9d83fdea68f05a")
        (version "1.7.1")
        (revision "0")
        (hash "1zymk8kzw1mvkasf0dryy2sbwxdr8ppr0a4j5r69y108dmvplqwn"))
    (package
      (name "emacs-auto-compile")
      (version (git-version version revision commit))
      (source (origin
                (method git-fetch)
                (uri (git-reference
                      (url "https://github.com/emacscollective/auto-compile.git")
                      (commit commit)))
                (sha256 (base32 hash))
                (file-name (git-file-name name version))))
      (build-system emacs-build-system)
      (propagated-inputs
       `(("emacs-packed" ,gnu:emacs-packed)))
      (home-page
       "https://github.com/emacscollective/auto-compile")
      (synopsis
       "automatically compile Emacs Lisp libraries")
      (description
       "")
      (license license:gpl3+))))

(define-public emacs-centaur-tabs-master
  (let ((commit "f4cef95acbd2eb99c8db3b6cdde74a6e0a966a0a")
        (version "3.1")
        (revision "101")
        (hash "10vpy22g2ccrj00kycrjcywywc69hqf3dm7vcbmmw7ralh9vclbc"))
    (package
      (name "emacs-centaur-tabs")
      (version (git-version version revision commit))
      (source (origin
                (method git-fetch)
                (uri (git-reference
                      (url "https://github.com/ema2159/centaur-tabs.git")
                      (commit commit)))
                (sha256 (base32 hash))
                (file-name (git-file-name name version))))
      (build-system emacs-build-system)
      (propagated-inputs
       `(("emacs-powerline" ,gnu:emacs-powerline)))
      (home-page
       "https://github.com/ema2159/centaur-tabs")
      (synopsis
       "Aesthetic, modern looking customizable tabs plugin")
      (description
       "Emacs plugin aiming to become an aesthetic, modern looking tabs plugin.")
      (license license:gpl3+))))

(define-public emacs-column-enforce-mode-master
  (let ((commit "14a7622f2268890e33536ccd29510024d51ee96f")
        (version "1.0.4")
        (revision "42")
        (hash "1vxra5vk78yns2sw89m41bggczqg1akq6xvzfs9kylhkg5yz3g7g"))
    (package
      (name "emacs-column-enforce-mode")
      (version (git-version version revision commit))
      (source (origin
                (method git-fetch)
                (uri (git-reference
                      (url "https://github.com/jordonbiondo/column-enforce-mode.git")
                      (commit commit)))
                (sha256 (base32 hash))
                (file-name (git-file-name name version))))
      (build-system emacs-build-system)
      (home-page
       "https://github.com/jordonbiondo/column-enforce-mode")
      (synopsis "Highlight text that extends beyond a certain column.")
      (description
       "Highlight text that extends beyond a certain column.  Can be
used to enforce 80 column rule (well more like suggest, not enforce)")
      (license license:gpl3+))))

(define-public emacs-eldoc-eval-master
  (let ((commit "e91800503c90cb75dc70abe42f1d6ae499346cc1")
        (version "1.1")
        (revision "17")
        (hash "01iklccpvd5n4jpmh0bhfl73a2q3cfk4q6dg70qx7ij87xg5pblf"))
    (package
      (name "emacs-eldoc-eval")
      (version (git-version version revision commit))
      (source (origin
                (method git-fetch)
                (uri (git-reference
                      (url "https://github.com/thierryvolpiatto/eldoc-eval.git")
                      (commit commit)))
                (sha256 (base32 hash))
                (file-name (git-file-name name version))))
      (build-system emacs-build-system)
      (home-page "https://github.com/thierryvolpiatto/eldoc-eval")
      (synopsis
       "Enable eldoc support when minibuffer is in use.")
      (description
       "This package enables eldoc support when minibuffer is in use.")
      (license license:gpl3+))))

(define-public emacs-smart-tabs-mode-master
  (let ((commit "1044c17e42479de943e69cdeb85e4d05ad9cca8c")
        (version "1.1")
        (revision "0")
        (hash "0hkgw9i4yynazx5vbkb8a1lfp0yndyi8c1w3cf7ajxpnig3hs9j6"))
    (package
      (name "emacs-smart-tabs-mode")
      (version (git-version version revision commit))
      (source (origin
                (method git-fetch)
                (uri (git-reference
                      (url "https://github.com/jcsalomon/smarttabs.git")
                      (commit commit)))
                (sha256 (base32 hash))
                (file-name (git-file-name name version))))
      (build-system emacs-build-system)
      (home-page
       "https://github.com/jcsalomon/smarttabs")
      (synopsis "Emacs smart tabs - indent with tabs, align with spaces!")
      (description
       "Emacs smart tabs - indent with tabs, align with spaces!")
      (license license:gpl2+))))

(define-public emacs-zig-mode-master
  (let ((commit "aa20d630b8c413dab8d6bd120ec3ed5db5c9da70")
        (version "0.0.8")
        (revision "112")
        (hash "0d081n6244q4y2sbhdya87gacw742v5f62pfs7c3bwgq0pcng997"))
    (package
      (name "emacs-zig-mode")
      (version (git-version version revision commit))
      (source (origin
                (method git-fetch)
                (uri (git-reference
                      (url "https://github.com/ziglang/zig-mode.git")
                      (commit commit)))
                (sha256 (base32 hash))
                (file-name (git-file-name name version))))
      (build-system emacs-build-system)
      ;; XXX run tests
      (home-page
       "https://ziglang.org")
      (synopsis "Emacs major mode for editing Zig files")
      (description
       "Emacs major mode for editing Zig files")
      (license license:gpl3+))))
