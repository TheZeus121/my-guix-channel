(define-module (ukko packages ruby)
  #:use-module ((gnu packages ruby) #:prefix gnu:)
  #:use-module (guix build-system ruby)
  #:use-module (guix download)
  #:use-module ((guix licenses) #:prefix license:)
  #:use-module (guix packages))

(define-public neocities
  (package
    (name "neocities")
    (version "0.0.15")
    (source
     (origin
       (method url-fetch)
       (uri (rubygems-uri "neocities" version))
       (sha256
        (base32 "08w8bbyal3rms068891w4ibspfh1qfb8cf2c3lz2yvjrsv118my3"))))
    (build-system ruby-build-system)
    (arguments `(#:tests? #f))
    (propagated-inputs
     `(("ruby-httpclient" ,gnu:ruby-httpclient)
       ("ruby-pastel" ,ruby-pastel-0.7.2)
       ("ruby-rake" ,gnu:ruby-rake)
       ("ruby-tty-prompt" ,ruby-tty-prompt-0.12.0)
       ("ruby-tty-table" ,ruby-tty-table-0.10.0)))
    (synopsis "Neocities.org CLI and API client")
    (description "Neocities.org CLI and API client")
    (home-page "https://neocities.org")
    (license license:expat)))

(define-public ruby-equatable-0.5.0
  (package
    (name "ruby-equatable")
    (version "0.5.0")
    (source
      (origin
        (method url-fetch)
        (uri (rubygems-uri "equatable" version))
        (sha256
          (base32 "1sjm9zjakyixyvsqziikdrsqfzis6j3fq23crgjkp6fwkfgndj7x"))))
    (build-system ruby-build-system)
    (arguments `(#:tests? #f))
    (synopsis
      "Provide equality comparison methods for objects based on their
attributes by generating implementations for the ==, eql?, hash and inspect
methods.")
    (description
      "Provide equality comparison methods for objects based on their
attributes by generating implementations for the ==, eql?, hash and inspect
methods.")
    (home-page "https://github.com/piotrmurach/equatable")
    (license license:expat)))

(define-public ruby-necromancer-0.4.0
  (package
    (name "ruby-necromancer")
    (version "0.4.0")
    (source
      (origin
        (method url-fetch)
        (uri (rubygems-uri "necromancer" version))
        (sha256
          (base32 "0v9nhdkv6zrp7cn48xv7n2vjhsbslpvs0ha36mfkcd56cp27pavz"))))
    (build-system ruby-build-system)
    (arguments `(#:tests? #f))
    (synopsis
      "Conversion from one object type to another with a bit of black magic.")
    (description
      "Conversion from one object type to another with a bit of black magic.")
    (home-page "https://github.com/piotrmurach/necromancer")
    (license license:expat)))

(define-public ruby-pastel-0.7.2
  (package
    (name "ruby-pastel")
    (version "0.7.2")
    (source
      (origin
        (method url-fetch)
        (uri (rubygems-uri "pastel" version))
        (sha256
          (base32 "1yf30d9kzpm96gw9kwbv31p0qigwfykn8qdis5950plnzgc1vlp1"))))
    (build-system ruby-build-system)
    (arguments `(#:tests? #f))
    (propagated-inputs
     `(("ruby-equatable" ,ruby-equatable-0.5.0)
       ("ruby-tty-color" ,ruby-tty-color-0.4.3)))
    (synopsis "Terminal strings styling with intuitive and clean API.")
    (description "Terminal strings styling with intuitive and clean API.")
    (home-page "https://ttytoolkit.org")
    (license license:expat)))

(define-public ruby-strings-0.1.8
  (package
    (name "ruby-strings")
    (version "0.1.8")
    (source
      (origin
        (method url-fetch)
        (uri (rubygems-uri "strings" version))
        (sha256
          (base32 "111876lcqrykh30w7zzkrl06d6rj9lq24y625m28674vgfxkkcz0"))))
    (build-system ruby-build-system)
    (arguments `(#:tests? #f))
    (propagated-inputs
      `(("ruby-strings-ansi" ,ruby-strings-ansi)
        ("ruby-unicode-display-width" ,gnu:ruby-unicode-display-width)
        ("ruby-unicode-utils" ,ruby-unicode-utils)))
    (synopsis
      "A set of methods for working with strings such as align, truncate, wrap
and many more.")
    (description
      "This package provides a set of methods for working with strings such as
align, truncate, wrap and many more.")
    (home-page "https://github.com/piotrmurach/strings")
    (license license:expat)))

(define-public ruby-strings-ansi
  (package
    (name "ruby-strings-ansi")
    (version "0.2.0")
    (source
      (origin
        (method url-fetch)
        (uri (rubygems-uri "strings-ansi" version))
        (sha256
          (base32 "120wa6yjc63b84lprglc52f40hx3fx920n4dmv14rad41rv2s9lh"))))
    (build-system ruby-build-system)
    (arguments `(#:tests? #f))
    (synopsis "Methods for processing ANSI escape codes in strings.")
    (description "Methods for processing ANSI escape codes in strings.")
    (home-page "https://github.com/piotrmurach/strings-ansi")
    (license license:expat)))

(define-public ruby-tty-color-0.4.3
  (package
    (name "ruby-tty-color")
    (version "0.4.3")
    (source
      (origin
        (method url-fetch)
        (uri (rubygems-uri "tty-color" version))
        (sha256
          (base32 "0zz5xa6xbrj69h334d8nx7z732fz80s1a0b02b53mim95p80s7bk"))))
    (build-system ruby-build-system)
    (arguments `(#:tests? #f))
    (synopsis "Terminal color capabilities detection")
    (description "Terminal color capabilities detection")
    (home-page "https://ttytoolkit.org")
    (license license:expat)))

(define-public ruby-tty-cursor-0.4.0
  (package
    (name "ruby-tty-cursor")
    (version "0.4.0")
    (source
      (origin
        (method url-fetch)
        (uri (rubygems-uri "tty-cursor" version))
        (sha256
          (base32 "07whfm8mnp7l49s2cm2qy1snhsqq3a90sqwb71gvym4hm2kx822a"))))
    (build-system ruby-build-system)
    (arguments `(#:tests? #f))
    (synopsis
      "The purpose of this library is to help move the terminal cursor around
and manipulate text by using intuitive method calls.")
    (description
      "The purpose of this library is to help move the terminal cursor around
and manipulate text by using intuitive method calls.")
    (home-page "https://ttytoolkit.org")
    (license license:expat)))

(define-public ruby-tty-prompt-0.12.0
  (package
    (name "ruby-tty-prompt")
    (version "0.12.0")
    (source
      (origin
        (method url-fetch)
        (uri (rubygems-uri "tty-prompt" version))
        (sha256
          (base32 "1026nyqhgmgxi2nmk8xk3hca07gy5rpisjs8y6w00wnw4f01kpv0"))))
    (build-system ruby-build-system)
    (arguments `(#:tests? #f))
    (propagated-inputs
     `(("ruby-necromancer" ,ruby-necromancer-0.4.0)
       ("ruby-pastel" ,ruby-pastel-0.7.2)
       ("ruby-tty-cursor" ,ruby-tty-cursor-0.4.0)
       ("ruby-wisper" ,ruby-wisper-1.6.1)))
    (synopsis
      "A beautiful and powerful interactive command line prompt with a robust
API for getting and validating complex inputs.")
    (description
      "This package provides a beautiful and powerful interactive command line
prompt with a robust API for getting and validating complex inputs.")
    (home-page "https://ttytoolkit.org")
    (license license:expat)))

(define-public ruby-tty-screen-0.6.5
  (package
    (name "ruby-tty-screen")
    (version "0.6.5")
    (source
      (origin
        (method url-fetch)
        (uri (rubygems-uri "tty-screen" version))
        (sha256
          (base32 "0azpjgyhdm8ycblnx9crq3dgb2x8yg454a13n60zfpsc0n138sw1"))))
    (build-system ruby-build-system)
    (arguments `(#:tests? #f))
    (synopsis
      "Terminal screen size detection which works on Linux, OS X and
Windows/Cygwin platforms and supports MRI, JRuby, TruffleRuby and Rubinius
interpreters.")
    (description
      "Terminal screen size detection which works on Linux, OS X and
Windows/Cygwin platforms and supports MRI, JRuby, TruffleRuby and Rubinius
interpreters.")
    (home-page "https://ttytoolkit.org")
    (license license:expat)))

(define-public ruby-tty-table-0.10.0
  (package
    (name "ruby-tty-table")
    (version "0.10.0")
    (source
      (origin
        (method url-fetch)
        (uri (rubygems-uri "tty-table" version))
        (sha256
          (base32 "05krrj1x5pmfbz74paszrsr1316w9b9jlc4wpd9s9gpzqfzwjzcg"))))
    (build-system ruby-build-system)
    (arguments `(#:tests? #f))
    (propagated-inputs
      `(("ruby-equatable" ,ruby-equatable-0.5.0)
        ("ruby-necromancer" ,ruby-necromancer-0.4.0)
        ("ruby-pastel" ,ruby-pastel-0.7.2)
        ("ruby-strings" ,ruby-strings-0.1.8)
        ("ruby-tty-screen" ,ruby-tty-screen-0.6.5)))
    (synopsis "A flexible and intuitive table generator")
    (description
      "This package provides a flexible and intuitive table generator")
    (home-page "https://ttytoolkit.org")
    (license license:expat)))

(define-public ruby-unicode-utils
  (package
    (name "ruby-unicode-utils")
    (version "1.4.0")
    (source
      (origin
        (method url-fetch)
        (uri (rubygems-uri "unicode_utils" version))
        (sha256
          (base32 "0h1a5yvrxzlf0lxxa1ya31jcizslf774arnsd89vgdhk4g7x08mr"))))
    (build-system ruby-build-system)
    (arguments `(#:tests? #f))
    (synopsis "additional Unicode aware functions for Ruby 1.9")
    (description "additional Unicode aware functions for Ruby 1.9")
    (home-page "http://github.com/lang/unicode_utils")
    (license #f)))                      ;TODO

(define-public ruby-wisper-1.6.1
  (package
    (name "ruby-wisper")
    (version "1.6.1")
    (source
      (origin
        (method url-fetch)
        (uri (rubygems-uri "wisper" version))
        (sha256
          (base32 "19bw0z1qw1dhv7gn9lad25hgbgpb1bkw8d599744xdfam158ms2s"))))
    (build-system ruby-build-system)
    (arguments `(#:tests? #f))
    (synopsis
      "A micro library providing objects with Publish-Subscribe capabilities.
Both synchronous (in-process) and asynchronous (out-of-process) subscriptions
are supported.  Check out the Wiki for articles, guides and examples:
https://github.com/krisleech/wisper/wiki")
    (description
      "A micro library providing objects with Publish-Subscribe capabilities.
Both synchronous (in-process) and asynchronous (out-of-process) subscriptions
are supported.  Check out the Wiki for articles, guides and examples:
https://github.com/krisleech/wisper/wiki")
    (home-page "https://github.com/krisleech/wisper")
    (license license:expat)))
