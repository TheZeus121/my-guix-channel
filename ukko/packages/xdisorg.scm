(define-module (ukko packages xdisorg)
  #:use-module (gnu packages terminals)
  #:use-module (gnu packages xdisorg)
  #:use-module (guix build-system cmake)
  #:use-module (guix git-download)
  #:use-module ((guix licenses) #:prefix license:)
  #:use-module (guix packages))

(define-public fmenu
  (package
    (name "fmenu")
    (version "0.1.0.0")
    (source (origin
              (method git-fetch)
              (uri (git-reference
                    (url "https://gitlab.com/TheZeus121/fmenu.git")
                    (commit version)))
              (sha256
               (base32
                "1xmqbhqbvzgv86r4n24h3n4g9mymh9c2nhp8wwxp7mzlgbibxj4k"))
              (file-name (git-file-name name version))))
    (build-system cmake-build-system)
    (propagated-inputs
     `(("go-github-com-junegunn-fzf" ,go-github-com-junegunn-fzf)
       ("rxvt-unicode" ,rxvt-unicode)
       ("xdotool" ,xdotool)))
    (arguments
     `(#:tests? #f))
    (synopsis "Dynamic menu")
    (description
     "A dynamic menu for X, based on the idea of dmenu, but using fzf
under the hood.")
    (home-page "https://hub.darcs.net/Ukko/fmenu")
    (license license:gpl3+)))
